const path = require('path');

export default {
  entry: 'src/index.js',
  publicPath: '/',
  // "theme": "./theme.config.js",
  extraBabelPlugins: [
    ['import', { 'libraryName': 'antd-mobile', 'style': true }]
  ],
  env: {
    development: {
      extraBabelPlugins: [ "dva-hmr" ],
    }
  },
  theme: './src/theme.js',
  html: { template: "./src/index.ejs" },
  proxy: {
    "/api": {
      "target": "http://127.0.0.1:8080/",
      "headers":{
        "host":"127.0.0.1"
      }
    }
  }
}