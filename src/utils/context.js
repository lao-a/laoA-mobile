import React from 'react';

export const TabBarContext = React.createContext({
  selectedItem: 0,
});

export function withTabBarContext(Comp, selectedItem) {
  return class extends React.PureComponent {
    render() {
      return (
        <TabBarContext.Provider value={{ selectedItem }}>
          <Comp {...this.props} />
        </TabBarContext.Provider>
      );
    }
  };
}
