import React from 'react';
import Style from './style.less';

export default (props) => {
  const handleClick = () => {
    if (props.onClick) {
      props.onClick();
    }
  };

  return (
    <span className={Style['x-avatar']} onClick={handleClick}>
      <img src={props.src} alt="" />
    </span>
  );
};
