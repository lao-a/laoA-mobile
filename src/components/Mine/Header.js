import React from 'react';
import { Flex, Badge } from 'antd-mobile';
import Avatar from './Avatar';
import Icon from '../common/Icon';
import Style from './style.less';

const toFixed = (n, s) => (new window.Number(n)).toFixed(s);

export default (props) => {
  const handlePersonClick = () => {
    if (props.onPersonClick) {
      props.onPersonClick();
    }
  };

  return (
    <div className={Style['mine-header']}>
      <Flex className={Style['mine-avatar']}>
        <div>
          <Avatar src={props.Avatar} />
        </div>
        <Flex.Item>
          <div className={Style['mine-title']} onClick={handlePersonClick}>
            <strong>{props.Name}</strong>
            <Badge
              text={props.Level}
              style={{
                marginTop: -20,
                padding: '0 3px',
                backgroundColor: 'transparent',
                borderRadius: 2,
                color: '#ff0',
                border: '1px solid #ff0',
              }}
            />
            <div style={{ marginTop: 8 }}><Icon name="icon-card" style={{ marginRight: 8 }} />{props.CardNum}</div>
          </div>
        </Flex.Item>
      </Flex>
      <Flex className={Style['mine-account']}>
        <Flex.Item>
          <div className={Style['account-context']}>{props.Points}</div>
          <div className={Style['account-tip']}>积分</div>
        </Flex.Item>
        <Flex.Item>
          <div className={Style['account-context']}>{toFixed(props.CashLeft, 2)}</div>
          <div className={Style['account-tip']}>现金</div>
        </Flex.Item>
        <Flex.Item>
          <div className={Style['account-context']}>{props.Liveness}</div>
          <div className={Style['account-tip']}>活跃度</div>
        </Flex.Item>
      </Flex>
    </div>
  );
};
