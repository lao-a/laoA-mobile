import React from 'react';
import { Flex, Icon } from 'antd-mobile';
import moment from 'moment';
import Style from './style.less';
import WaveProgress from '../WaveProgress';

const isEmpty = p => (!p || p.length < 3);

// 封面图
const ActCover = (props) => {
  return (
    <div className={Style['act-thumb']}>
      <img src={props.thumb} alt="" />
    </div>
  );
};

// 头部
const ActHead = (props) => {
  return (
    <div className={Style['act-head']}>
      <Flex>
        <Flex.Item>
          <h2>{props.title}</h2>

          <Flex className={Style['act-tips']}>
            <Flex.Item>
              <Icon type="datetime" size="xs" />
              {moment(props.date).format('YYYY-MM-DD')}
            </Flex.Item>
            <Flex.Item>
              <Icon type="location" size="xs" />
              {props.loc}
            </Flex.Item>
          </Flex>
        </Flex.Item>
        <div style={{ padding: '0 1rem' }}>
          <WaveProgress
            progress={30}
            motion={false}
            fontSize={'72px'}
            className={Style['wave-progress']}
          />
        </div>
      </Flex>
    </div>
  );
};

// Detail
const ActDetail = (props) => {
  return (
    <div className={Style['act-body']}>
      <h2>{props.title}</h2>
      <div className={Style['act-detail']} dangerouslySetInnerHTML={{ __html: props.html }} />
    </div>
  );
};

class ActivityDetail extends React.Component {

  /* eslint-disable no-undef */
  handleRegBtnClick = () => {
    // go to register
    this.props.onApply();
  }

  render() {
    const detail = this.props.detail || {};

    return (
      <div>
        <div className={Style['act-container']}>
          <ActCover thumb={detail.Cover || ''} />
          <div className={Style['act-main']}>
            <ActHead
              title={detail.Title}
              date={detail.StartDate}
              loc={detail.Location}
            />
          </div>
          {
            !isEmpty(detail.Special) &&
            <ActDetail
              title="行程亮点 HIGHLIGHTS"
              html={detail.Special}
            />
          }
          {
            !isEmpty(detail.BasicInfo) &&
            <ActDetail
              title="基本信息 Basic Information"
              html={detail.BasicInfo}
            />
          }
          {
            !isEmpty(detail.TravelPlan) &&
            <ActDetail
              title="活动行程 Travel Arrangements"
              html={detail.TravelPlan}
            />
          }
          {
            !isEmpty(detail.AboutFee) &&
            <ActDetail
              title="费用说明 Expense Explanation"
              html={detail.AboutFee}
            />
          }
          {
            !isEmpty(detail.Contact) &&
            <ActDetail
              title="报名方式 Join In"
              html={detail.Contact}
            />
          }
        </div>
      </div>
    );
  }
}

export default ActivityDetail;
