import React from 'react';
import { Flex, List, InputItem, TextareaItem, Button } from 'antd-mobile';
import { createForm } from 'rc-form';
import VerticalPage from '../Layout/VerticalPage';

const ApplyForm = (props) => {
  const { getFieldProps } = props.form;
  const member = props.Member || {};

  const handleApply = () => {
    props.form.validateFields((err, val) => {
      if (props.onApply) {
        props.onApply(err, val);
      }
    });
  };

  const handleCancel = () => {
    if (props.onCancel) {
      props.onCancel();
    }
  };

  return (
    <VerticalPage style={{ height: '100%' }}>
      <VerticalPage.Body>
        <List>
          <InputItem
            {...getFieldProps('UserName', {
              initialValue: member.UserName,
            })}
            clear
            placeholder="请填写姓名"
          >姓名</InputItem>
          <InputItem
            {...getFieldProps('Phone', {
              initialValue: member.Phone,
            })}
            clear
            placeholder="请填写手机号"
          >手机号</InputItem>
          <TextareaItem
            title="备注"
            {...getFieldProps('Remark')}
            autoHeight
            rows={3}
            placeholder="请填写备注"
          />
        </List>
      </VerticalPage.Body>
      <VerticalPage.Footer>
        <Flex>
          <Flex.Item>
            <Button type="ghost" style={{ borderRadius: 0 }} onClick={handleCancel}>取消</Button>
          </Flex.Item>
          <Flex.Item style={{ margin: 0 }}>
            <Button type="primary" style={{ borderRadius: 0 }} onClick={handleApply}>确定</Button>
          </Flex.Item>
        </Flex>
      </VerticalPage.Footer>
    </VerticalPage>
  );
};

export default createForm()(ApplyForm);
