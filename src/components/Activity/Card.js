import React from 'react';
import { WingBlank, Card } from 'antd-mobile';
import moment from 'moment';
import Style from './style.less';
import Icon from '../common/Icon';

export default (props) => {
  const data = props.data || {};

  return (
    <WingBlank size="md">
      <Card className={Style['scenic-card']} full style={{ boxShadow: '0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12)' }}>
        <Card.Body style={{ padding: 0 }}>
          <div className={Style['scenic-thumb']} style={{ backgroundImage: `url(${data.Cover})` }} />
          <div className={Style['scenic-title']}>{data.Title}</div>
        </Card.Body>
        <Card.Footer
          content={(
            <div className={Style['scenic-tips']}>
              <span><Icon name="icon-map" />{data.Location || ''}</span>
              <span><Icon name="icon-time" />{(moment(data.StartDate).format('YYYY-MM-DD'))}</span>
            </div>
          )}
          extra={(
            <div className={Style['scenic-tips']}>
              <span><Icon name="icon-heart-o" />{(data.TotalCusts || 0) - (data.LeftCusts || 0)}</span>
              <span><Icon name="icon-money" />{data.FeeBrief || '0.0'}</span>
            </div>
          )}
        />
      </Card>
    </WingBlank>
  );
};
