import React from 'react';
import { Grid } from 'antd-mobile';
import Icon from '../common/Icon';

export default (props) => {
  const data = props.data || [];

  const handleClick = (item) => {
    if (props.onClick) {
      props.onClick(item);
    }
  };

  return (
    <Grid
      data={data}
      hasLine={false}
      columnNum={props.columnNum || 4}
      onClick={handleClick}
      renderItem={(item) => {
        const iconStyle = { fontSize: '2em', color: item.Color };

        return (
          <div style={{ padding: '12.5px', fontSize: '1rem' }}>
            <Icon name={item.icon} style={iconStyle} alt="" />
            <div style={{ fontSize: '0.8em', marginTop: '12px', color: '#333' }}>
              <span>{item.text}</span>
            </div>
          </div>
        );
      }}
    />
  );
};
