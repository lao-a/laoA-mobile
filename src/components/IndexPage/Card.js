import React from 'react';
import { Flex } from 'antd-mobile';

export default (props) => {
  const items = props.items || [];
  const keyOf = inx => `index-cardlist-${inx}`;
  const handleClick = item => () => {
    if (props.onClick) {
      props.onClick(item);
    }
  };
  const handleMoreClick = type => () => {
    if (props.onMoreClick) {
      props.onMoreClick(type);
    }
  };

  const borderStyle = '1px solid #ccc';

  return (
    <div style={{ backgroundColor: '#fff', fontSize: '.8rem', padding: '0 .8rem', boxSizing: 'border-box' }}>
      <Flex style={{ borderBottom: borderStyle, padding: '.4rem 0' }}>
        <Flex.Item>{props.header}</Flex.Item>
        <div style={{ width: '20%', textAlign: 'right' }} onClick={handleMoreClick(props.type)}>{props.more}</div>
      </Flex>
      <Flex>
        {
          items.map((item, inx) => {
            return (
              <Flex.Item
                key={keyOf(inx)}
                onClick={handleClick(item)}
              >
                {item.content}
              </Flex.Item>
            );
          })
        }
      </Flex>
    </div>
  );
};
