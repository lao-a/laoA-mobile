import React from 'react';
import { WhiteSpace, Flex } from 'antd-mobile';
import Icon from '../common/Icon';
import Card from './Card';

const times = n => ' '.repeat(n).split('');
const keyGen = (p, i) => `${p}-${i}`;

const ActivityCard = (props) => {
  const { data, limit, type } = props;
  const iconStyle = { color: type.Color, fontSize: '1.2rem' };

  const header = (
    <div>
      <Icon name={type.Icon} style={iconStyle} />
      <span>{` ${type.TypeName}`}</span>
    </div>
  );
  const more = (<span>更多 <Icon name="icon-more" /></span>);
  const items = data && data.length
    ? times(data.length > limit ? limit : data.length).map((_, inx) => {
      const item = data[inx];
      const imgStyle = {
        height: '8rem',
        backgroundImage: `url('${item.Cover}')`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '0% 50%',
      };

      const content = (
        <div key={keyGen('index-act', inx)} style={{ boxSizing: 'border-box' }}>
          <WhiteSpace size="md" />
          <div style={imgStyle} />
          <WhiteSpace size="md" />
          <Flex style={{ padding: '0 .5rem' }}>
            <Flex.Item>{item.Location}</Flex.Item>
            <Flex.Item style={{ textAlign: 'right' }}>{item.FeeBrief}</Flex.Item>
          </Flex>
          <WhiteSpace size="md" />
        </div>
      );

      return { ...item, content };
    })
    : [{ content: (
      <div
        style={{
          textAlign: 'center',
          color: '#888',
          padding: '1rem',
        }}
      >暂无数据</div>) }];

  return (
    <Card
      header={header}
      more={more}
      type={type}
      items={items}
      onClick={props.onClick}
      onMoreClick={props.onMoreClick}
    />
  );
};

export default ActivityCard;
