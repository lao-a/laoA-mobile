import CommPage from './CommPage';
import TabBarPage from './TabBarPage';
import VerticalPage from './VerticalPage';

export {
  CommPage,
  TabBarPage,
  VerticalPage,
};
