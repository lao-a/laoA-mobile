import React from 'react';
import { Helmet } from 'react-helmet';
import TabBar from '../common/TabBar';
import { withTabBarContext } from '../../utils/context';
import tabBars from '../../config/tabbar';

const pageStyle = {
  width: '100%',
  height: '100%',
  position: 'fixed',
  top: 0,
};

class TabBarPage extends React.Component {
  /* eslint-disable no-undef */
  static withContext = withTabBarContext;

  render() {
    return (
      <div style={{ ...pageStyle, ...(this.props.style || {}) }}>
        <Helmet>
          <meta charSet="utf-8" />
          <title>{this.props.title || ''}</title>
        </Helmet>
        <TabBar data={tabBars}>
          {this.props.children}
        </TabBar>
      </div>
    );
  }
}

export default TabBarPage;
