import React from 'react';
import { Flex } from 'antd-mobile';
import { Helmet } from 'react-helmet';

const fullWidth = { width: '100%' };
const noMargin = { margin: 0 };

const VerticalPage = (props) => {
  return (
    <Flex style={fullWidth} {...props} direction="column">
      <Helmet>
        <meta charSet="utf-8" />
        <title>{props.title || ''}</title>
      </Helmet>
      {props.children}
    </Flex>
  );
};

const Item = (props) => {
  return (<Flex.Item style={{ ...fullWidth, ...noMargin }} {...props}>{props.children}</Flex.Item>);
};

const Content = (props) => {
  return (<div style={{ ...fullWidth, ...noMargin }} {...props}>{props.children}</div>);
};


VerticalPage.Item = Item;
VerticalPage.Content = Content;
VerticalPage.Header = Content;
VerticalPage.Footer = Content;
VerticalPage.Body = Item;

export default VerticalPage;
