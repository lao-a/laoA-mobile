import React from 'react';
import { Helmet } from 'react-helmet';

const fullScreen = { width: '100%', height: '100%' };

class CommPage extends React.Component {
  render() {
    return (
      <div style={{ ...fullScreen, ...(this.props.style || {}) }}>
        <Helmet>
          <meta charSet="utf-8" />
          <title>{this.props.title || ''}</title>
        </Helmet>
        {this.props.children}
      </div>
    );
  }
}

export default CommPage;
