import React from 'react';
import { List, InputItem, TextareaItem, Button, Toast } from 'antd-mobile';
import { createForm } from 'rc-form';
import VerticalPage from '../Layout/VerticalPage';

const MemTypePerson = 'person';

const isEmpty = (val, msg) => {
  if (!val) {
    Toast.info(msg, 2);
  }

  return !val;
};

const formateDate = dateStr => `${dateStr.substr(0, 4)}-${dateStr[4]}${dateStr[5]}-${dateStr.substr(-2)}`;

class MemberDetail extends React.Component {
  /* eslint-disable no-undef */
  handleSubmit = () => {
    const { validateFields } = this.props.form;
    validateFields((err, values) => {
      if (err) {
        // todo
      } else {
        if (isEmpty(values.Name, '请填写您的姓名')) return undefined;
        if (isEmpty(values.NickyName, '请填写您的昵称')) return undefined;
        if (isEmpty(values.IdCard, '请填写您的身份证号')) return undefined;
        if (isEmpty(values.Phone, '请填写您的手机号')) return undefined;

        if (this.props.onSubmit) {
          this.props.onSubmit(values);
        }
      }
    });
  }

  /* eslint-disable no-undef */
  handleIdCardChange = (e) => {
    const val = e.target.values;

    if (val.length === 18) {
      this.props.form.setFieldsValue({
        Birthday: formateDate(val.substr(6, 8)),
      });
    }
  }

  render() {
    const { getFieldProps } = this.props.form;
    const detail = this.props.detail || {};
    const editable = this.props.editable === false;

    // 部分隐藏字段
    getFieldProps('MemType', { initialValue: MemTypePerson });
    getFieldProps('Birthday', { initialValue: detail.Birthday });

    return (
      <VerticalPage>
        <VerticalPage.Body>
          <List renderHeader={(<h2 style={{ textAlign: 'center', fontWeight: 400 }}>欢迎加入我们!</h2>)}>
            <InputItem
              {...getFieldProps('Name', { initialValue: detail.Name })}
              clear
              editable={editable}
              placeholder="请输入您的姓名"
              extra="必填"
            >姓名</InputItem>
            <InputItem
              {...getFieldProps('NickyName', { initialValue: detail.NickyName })}
              clear
              editable={editable}
              placeholder="请输入您的昵称"
              extra="必填"
            >昵称</InputItem>
            <InputItem
              {...getFieldProps('IdCard', {
                initialValue: detail.IdCard,
                onChange: this.handleIdCardChange,
              })}
              clear
              editable={editable}
              placeholder="请输入您的身份证号"
              extra="必填"
            >身份证号</InputItem>
            <InputItem
              {...getFieldProps('Passport', { initialValue: detail.Passport })}
              clear
              editable={editable}
              placeholder="请输入您的护照号"
            >护照号</InputItem>
            <InputItem
              type="phone"
              {...getFieldProps('Phone', { initialValue: detail.Phone })}
              clear
              editable={editable}
              placeholder="请输入您的手机号"
              extra="必填"
            >手机号</InputItem>
            <InputItem
              {...getFieldProps('Profession', { initialValue: detail.Profession })}
              clear
              editable={editable}
              placeholder="请输入您的职业"
            >职业</InputItem>
          </List>
          <List renderHeader="联系地址">
            <TextareaItem
              editable={editable}
              {...getFieldProps('Address', { initialValue: detail.Address })}
              rows={3}
              placeholder="请输入您的联系地址"
            />
          </List>
          <List renderHeader="过敏史/特殊病史">
            <TextareaItem
              editable={editable}
              {...getFieldProps('DiseaseHistory', { initialValue: detail.DiseaseHistory })}
              rows={3}
              placeholder="如果有, 请尽量填写, 这对您和我们都非常有帮助"
            />
          </List>
          <List renderHeader="兴趣爱好">
            <TextareaItem
              editable={editable}
              {...getFieldProps('Interests', { initialValue: detail.Interests })}
              rows={3}
              placeholder="请输入您的兴趣爱好"
            />
          </List>
          <List renderHeader="备 注">
            <TextareaItem
              editable={editable}
              {...getFieldProps('Remark', { initialValue: detail.Remark })}
              rows={3}
              placeholder="请输入您的备 注"
            />
          </List>
        </VerticalPage.Body>
        <VerticalPage.Footer>
          <Button type="primary" style={{ borderRadius: 0 }} onClick={this.handleSubmit}>确定</Button>
        </VerticalPage.Footer>
      </VerticalPage>
    );
  }
}

export default createForm()(MemberDetail);
