import React from 'react';
import { Result, Icon } from 'antd-mobile';

const iconSize = { width: '60px', height: '60px' };

export default (props) => {
  const success = () => <Icon type="check-circle" color="#1F90E6" style={{ ...iconSize }} />;
  const error = () => <Icon type="cross-circle-o" color="#F13642" style={{ ...iconSize }} />;

  const icon = props.success
    ? success
    : (
      props.error ? error : () => undefined
    );

  return (
    <Result
      img={icon()}
      title={props.title}
      message={props.message}
    />
  );
};
