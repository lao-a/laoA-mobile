import React from 'react';

// 图标, 只能使用 iconfont 上项目支持的
// http://www.iconfont.cn/manage/index?manage_type=myprojects&projectId=511767
export default (props) => {
  const { name, style } = props || {};
  const className = `iconfont ${name}`;

  return <i className={className} style={style} />;
};
