import React from 'react';
import { TabBar } from 'antd-mobile';
import { TabBarContext } from '../../utils/context';

export default class extends React.PureComponent {
  render() {
    const data = this.props.data || [];

    return (
      <TabBarContext.Consumer>
        {
          ({ selectedItem: current }) => (
            <TabBar>
              {
                data.map((item, inx) => {
                  return (
                    <TabBar.Item
                      key={`tab-${inx}`}
                      {...item}
                      selected={inx === current}
                    >
                      {
                        inx === current
                          ? this.props.children
                          : <div />
                      }
                    </TabBar.Item>
                  );
                })
              }
            </TabBar>
          )
        }
      </TabBarContext.Consumer>
    );
  }
}

