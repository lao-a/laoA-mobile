import app from './app';
import './index.css';

// 2. Plugins
// app.use({});

// 3. Model
// app.model(require('./models/app').default);

// 4. Router
app.router(require('./router').default);

// 5. Start
app.start('#root');
