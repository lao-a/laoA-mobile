import { Modal } from 'antd-mobile';

export default {
  namespace: 'base',

  state: {
    detail: {},
    list: [],
    pageNavi: [], // 每个元素为 { id: 'xx', current: 0, perpage: 10 }
  },

  effects: {
    *selectTabBar({ payload }, { put }) {
      const { tabBarSelected } = payload;
      yield put({ type: 'app/syncState', payload: { tabBarSelected } });
    },
  },

  reducers: {
    syncState(state, { payload }) {
      return { ...state, ...payload };
    },

    syncPageNavi(state, { payload }) {
      const { pageNavi: oriSetting } = state;
      const pageNavi = oriSetting.filter(x => x.id !== payload.id).concat(payload);
      return { ...state, pageNavi };
    },

    appendList(state, { payload }) {
      return { ...state, list: [...state.list, payload] };
    },

    alert(state, { payload }) {
      const { title, message, callback } = payload || {};
      const onPress = callback && typeof callback === 'function' ? callback : () => {};

      Modal.alert(title, message, [{ text: '确定', onPress }]);
      return { ...state };
    },
  },

};
