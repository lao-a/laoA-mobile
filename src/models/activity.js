import modelExtend from 'dva-model-extend';
import pathToRegexp from 'path-to-regexp';
import { statusOk } from '../utils/httpcode';
import { searchParams } from '../utils';
import { interact } from '../config/api';
import base from './base';

export default modelExtend(base, {
  namespace: 'activity',

  state: {
    result: {},
    types: [],
  },

  effects: {
    *loadTypes(_, { call, put }) {
      const { code, message: result } = yield call(interact('getActivityTypes'));
      if (code === statusOk) {
        yield put({ type: 'syncState', payload: { types: result.ActivityTypes } });
      } else {
        throw new Error(result);
      }
    },

    *loadList({ payload }, { call, put }) {
      // payload 为url search
      // 暂时只支持 page
      const page = payload.page || 0;
      const { code, message: result } = yield call(interact('getActivityList'), { page });

      if (code === statusOk) {
        yield put({ type: 'syncState', payload: { list: result.Activities } });
        // yield put({ type: 'syncPageNavi', payload: { id: PAGE_LIST, current: page } });
      } else {
        throw new Error(result);
      }

      // 获取分类
      yield put({ type: 'loadTypes' });
    },

    *loadEdit({ payload }, { call, put }) {
      if (payload) {
        const { code, message: result } = yield call(interact('getActivity', [payload]));

        if (code === statusOk) {
          if (!result.Activity.ActivityId) {
            throw new Error('没有找到对应的活动');
          }

          yield put({ type: 'syncState', payload: { detail: result.Activity } });
        } else {
          throw new Error(result);
        }
      }

      // 获取分类
      yield put({ type: 'loadTypes' });
    },

    *join({ payload }, { call }) {
      const { params, success, fail } = payload;

      const { code, message: result } = yield call(interact('joinActivity', [params.ActivityId]), params);
      if (code === statusOk) {
        success();
      } else {
        fail(result);
      }
    },
  },

  reducers: {
    updateResult(state, { payload }) {
      const result = { ...state.result, ...payload };
      return { ...state, result };
    },
  },

  subscriptions: {
    setup({ history, dispatch }) {
      history.listen((location) => {
        let match = pathToRegexp('/activity').exec(location.pathname);
        if (match) {
          dispatch({ type: 'loadList', payload: searchParams(location.search) });
        }

        match = pathToRegexp('/activity/:actId?').exec(location.pathname);
        if (match) {
          dispatch({ type: 'loadEdit', payload: match[1] });
        }
      });
    },
  },
});
