import modelExtend from 'dva-model-extend';
import base from './base';
import { statusOk } from '../utils/httpcode';
import { interact } from '../config/api';

export default modelExtend(base, {
  namespace: 'app',

  state: {
    user: {
      CardNum: 'x123456',
      MemId: 1,
      Name: '张三',
      Avatar: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1510500309136&di=75ee2acb8b196f384f39930b71f8f509&imgtype=0&src=http%3A%2F%2Fimg.dongqiudi.com%2Fuploads%2Favatar%2F2015%2F07%2F25%2FQM387nh7As_thumb_1437790672318.jpg',
      Points: 100000,
      CashLeft: 999.05,
      Level: '黄金会员',
      Liveness: 100,
    },
    tabBarSelected: 0,
    SliderItems: [],
    ActivityTypes: [],
    Activities: [],
  },

  effects: {
    *appInit({ payload }, { call, put }) {  // eslint-disable-line
      const { code, message: result } = yield call(interact('appIndex'));
      if (code === statusOk) {
        yield put({ type: 'syncState', payload: result });
      } else {
        throw new Error(result);
      }
    },
  },

  subscriptions: {
    setup({ history, dispatch }) {
      history.listen((location) => {
        if (location.pathname === '/') {
          dispatch({ type: 'appInit' });
        }
      });
    },
  },
});
