import React from 'react';
import Icon from '../components/common/Icon';
import { redirect } from '../utils';

// https://mobile.ant.design/components/tab-bar-cn/#TabBar.Item
const tabBars = [
  {
    icon: <Icon name="icon-home-o" />,
    selectedIcon: <Icon name="icon-home" style={{ color: '#1890ff' }} />,
    title: '首页',
    selected: true,
    onPress: () => redirect('index'),
  },
  {
    icon: <Icon name="icon-huodong-o" />,
    selectedIcon: <Icon name="icon-huodong" style={{ color: '#1890ff' }} />,
    title: '活动',
    selected: false,
    onPress: () => redirect('activity'),
  },
  {
    icon: <Icon name="icon-mine-o" />,
    selectedIcon: <Icon name="icon-mine" style={{ color: '#1890ff' }} />,
    title: '我的',
    selected: false,
    onPress: () => redirect('mine'),
  },
];

export default tabBars;
