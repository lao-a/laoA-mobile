import services from '../utils/services';

const prefix = '/api';

const sysApis = {
  appIndex: {
    type: 'GET',
    url: `${prefix}/app/index`,
  },
  getActivityList: {
    type: 'GET',
    url: `${prefix}/activity`,
  },
  getActivity: {
    type: 'GET',
    url: `${prefix}/activity/:actId`,
  },
  joinActivity: {
    type: 'PUT',
    url: `${prefix}/activity/:actId/join`,
  },
  getActivityTypes: {
    type: 'GET',
    url: `${prefix}/dict/activity/type`,
  },
  newCardNum: {
    type: 'POST',
    url: `${prefix}/cardnum`,
  },
  getMemberTypes: {
    type: 'GET',
    url: `${prefix}/dict/member/type`,
  },
  getMemberLevels: {
    type: 'GET',
    url: `${prefix}/dict/member/level`,
  },
  joinMember: {
    type: 'POST',
    url: `${prefix}/member/join`,
  },
  getMine: {
    type: 'GET',
    url: `${prefix}/member/mine`,
  },
  updateMine: {
    type: 'POST',
    url: `${prefix}/member/mine`,
  },
};

// 用数组替换url中的参数部分
const replaceUrl = (source, replacement) => {
  if (!replacement || !replacement.length) return source;

  let i = -1;
  return source.replace(/:[^/]+/g, () => {
    i += 1;
    return replacement[i];
  });
};

// () => () => {}
export const interact = (api, ...args) => {
  const { type, url } = sysApis[api];
  const target = replaceUrl(url, args);

  return (data) => {
    return services[type](target, data);
  };
};

export default sysApis;

