import React from 'react';
import { connect } from 'dva';
import { WhiteSpace } from 'antd-mobile';
import { TabBarPage } from '../../components/Layout';
import Slider from '../../components/IndexPage/Slider';
import IconList from '../../components/IndexPage/IconList';
import ActivityCard from '../../components/IndexPage/ActivityCard';
import { getRoute } from '../../config/route';
import { redirect } from '../../utils';

const keyGen = (p, i) => `${p}-${i}`;

class IndexPage extends React.Component {
  render() {
    const { ActivityTypes, Activities, SliderItems } = this.props;
    const iconData = (ActivityTypes || []).map((type) => {
      return { ...type, icon: type.Icon, text: type.TypeName };
    });
    const sliders = (SliderItems || []).map((item) => {
      const link = getRoute({ name: 'activity.detail', params: { id: item.ActivityId } });
      return { ...item, link, image: item.Cover };
    });

    const handleTypeClick = (type) => {
      if (!type) return undefined;

      redirect({ name: 'activity', query: { type: type.TypeId } });
    };

    const handleItemClick = (item) => {
      if (!item) return undefined;

      redirect({ name: 'activity.detail', params: { id: item.ActivityId } });
    };

    return (
      <TabBarPage title="AceClub">
        <Slider autoplay infinite items={sliders} />
        <IconList data={iconData} onClick={handleTypeClick} />
        {
          (ActivityTypes || []).map((type, inx) => {
            // 每 3 个一循环
            // 每个单次列表是 2, 2, 1 结构
            const limit = (inx + 1) % 3 === 0 ? 1 : 2;
            const acts = (Activities || []).filter(x => x.TypeId === type.TypeId);

            return (
              <div key={keyGen('index-listcard', inx)}>
                <WhiteSpace size="lg" />
                <ActivityCard
                  data={acts}
                  type={type}
                  limit={limit}
                  onClick={handleItemClick}
                  onMoreClick={handleTypeClick}
                />
              </div>
            );
          })
        }
      </TabBarPage>
    );
  }
}

export default TabBarPage.withContext(connect(s => s.app)(IndexPage), 0);
// export default connect(s => s.app)(IndexPage);
