import React from 'react';
import { connect } from 'dva';
import { withRouter } from 'dva/router';

const App = (props) => {
  return (
    <div style={{ height: '100%' }}>{props.children}</div>
  );
};

export default withRouter(connect(s => s.app)(App));
