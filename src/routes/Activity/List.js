import React from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { WhiteSpace, Flex, Picker, PullToRefresh } from 'antd-mobile';
import { TabBarPage } from '../../components/Layout';
import Card from '../../components/Activity/Card';
import { getRoute } from '../../config/route';
import { searchParams } from '../../utils';
import Style from './style.less';

const keyGen = (f, b) => `${f}-${b}`;

const PickerChildren = (props) => {
  return (
    <span onClick={props.onClick}>
      {props.children}
    </span>
  );
};

class ActivityList extends React.Component {
  constructor(props) {
    super(props);

    const tabId = searchParams(this.props.location.search).type;

    this.state = {
      loadding: false,
      tabId,
    };
  }

  /* eslint-disable no-undef */
  handleTabChange = (tab) => {
    this.setState({ tabId: tab.tabId });
  }

  render() {
    const types = (this.props.types || []).map(x => ({ value: x.TypeId, label: x.TypeName }));

    return (
      <TabBarPage title="活动列表">
        <Flex className={Style['act-list-bar']}>
          <Flex.Item className={Style['act-list-bar-item']}>
            <div className={Style['act-bar-picker']}>
              <Picker data={types} title="请选择类型">
                <PickerChildren>类型</PickerChildren>
              </Picker>
            </div>
          </Flex.Item>
          <Flex.Item className={Style['act-list-bar-item']}>
            <div className={Style['act-bar-picker']}>
              <Picker data={types} title="请选择类型">
                <PickerChildren>天数</PickerChildren>
              </Picker>
            </div>
          </Flex.Item>
          <Flex.Item className={Style['act-list-bar-item']}>
            <div className={Style['act-bar-picker']}>
              <Picker data={types} title="请选择类型">
                <PickerChildren>时间</PickerChildren>
              </Picker>
            </div>
          </Flex.Item>
          <Flex.Item className={Style['act-list-bar-item']}>
            <div className={Style['act-bar-picker']}>
              <Picker data={types} title="请选择类型">
                <PickerChildren>目的地</PickerChildren>
              </Picker>
            </div>
          </Flex.Item>
        </Flex>
        <div>
          <PullToRefresh
            direction="up"
            refreshing={this.state.loadding}
            onRefresh={this.handleRefresh}
          >
            {
              (this.props.list || []).map((it, inx) => {
                return (
                  <Link to={getRoute({ name: 'activity.detail', params: { id: it.ActivityId } })} key={keyGen('item', inx)}>
                    <WhiteSpace size="md" />
                    <Card data={it} />
                  </Link>
                );
              })
            }
            <WhiteSpace size="lg" />
          </PullToRefresh>
        </div>
      </TabBarPage>
    );
  }
}

export default TabBarPage.withContext(connect(s => s.activity)(ActivityList), 1);
