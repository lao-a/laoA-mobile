import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Button, Modal, Toast } from 'antd-mobile';
import { CommPage } from '../../components/Layout';
import Detail from '../../components/Activity/Detail';
import Apply from '../../components/Activity/Apply';
import { redirect } from '../../utils';

class ActivityDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showApply: false,
      feedback: {},
    };
  }

  /* eslint-disable no-undef */
  handleApplySubmit = (err, val) => {
    if (err) {
      Toast.fail('报名信息不正确, 请确认');
      return undefined;
    }

    const fail = (e) => {
      this.props.dispatch({
        type: 'activity/updateResult',
        payload: {
          error: true,
          title: '报名失败',
          message: e,
        },
      });
      this.setState({ showApply: false });
      redirect('activity.result');
    };

    const success = () => {
      this.props.dispatch({
        type: 'activity/updateResult',
        payload: {
          success: true,
          title: '报名成功',
          message: '是否加入本站会员俱乐部, 享受超多打折优惠?',
          handleOk: () => redirect('member.join'),
        },
      });
      this.setState({ showApply: false });
      redirect('activity.result');
    };

    const payload = {
      params: val,
      success,
      fail,
    };

    this.props.dispatch({ type: 'activity/join', payload });
  }

  /* eslint-disable no-undef */
  handleRegBtnClick = () => {
    // // goto register
    // this.props.dispatch(routerRedux.push('/'));
    this.setState({ showApply: true });
  }

  render() {
    const detail = this.props.detail || {};

    return (
      <CommPage title="活动详情">
        <Detail detail={detail} />
        <Modal
          popup
          animationType="slide-up"
          visible={this.state.showApply}
        >
          <div style={{ height: '300px' }}>
            <Apply
              onApply={this.handleApplySubmit}
              onCancel={() => this.setState({ showApply: false })}
            />
          </div>
        </Modal>
        {
          !moment(detail.StartDate).isAfter(moment()) &&
          (<div style={{ width: '100%', position: 'fixed', bottom: 0 }}>
            <Button type="primary" style={{ borderRadius: 0 }} onClick={this.handleRegBtnClick}>
              马上报名
            </Button>
          </div>)
        }
      </CommPage>
    );
  }
}

export default connect(s => s.activity)(ActivityDetail);
