import React from 'react';
import { connect } from 'dva';
import { Button } from 'antd-mobile';
import { CommPage } from '../../components/Layout';
import Result from '../../components/common/Result';

const ActivityResult = (props) => {
  const result = props.result || {};

  return (
    <CommPage title={result.title}>
      <Result {...result} />
      {
        result.handleOk &&
        !result.handleCancel &&
        (
          <div style={{ margin: '1rem auto', width: '90%' }}>
            <Button type="primary" onClick={result.handleOk}>是</Button>
          </div>
        )
      }
      {
        result.handleOk &&
        result.handleCancel &&
        (
          <div style={{ margin: '1rem auto', width: '90%' }}>
            <Button type="primary" onClick={result.handleOk} style={{ marginRight: '4px' }}>是</Button>
            <Button type="warning" onClick={result.handleCancel}>否</Button>
          </div>
        )
      }
    </CommPage>
  );
};

export default connect(s => s.activity)(ActivityResult);
