import React from 'react';
import { List } from 'antd-mobile';
import { connect } from 'dva';
import { TabBarPage } from '../../components/Layout';
import Header from '../../components/Mine/Header';
import Styles from './style.less';

class Mine extends React.Component {
  render() {
    const user = this.props.mine || {};

    return (
      <TabBarPage title="我的">
        <Header {...user} />
        <div>
          <div className={Styles['mine-items']}>
            <List>
              <List.Item
                arrow="horizontal"
                onClick={x => x}
              >订单</List.Item>
              <List.Item
                arrow="horizontal"
                onClick={x => x}
              >优惠券</List.Item>
            </List>
          </div>
          <div className={Styles['mine-items']}>
            <List>
              <List.Item
                arrow="horizontal"
                onClick={x => x}
              >游记</List.Item>
              <List.Item
                arrow="horizontal"
                onClick={x => x}
              >分享</List.Item>
            </List>
          </div>
          <div className={Styles['mine-items']}>
            <List>
              <List.Item
                arrow="horizontal"
                onClick={x => x}
              >常用联系人</List.Item>
            </List>
          </div>
        </div>
      </TabBarPage>
    );
  }
}

export default TabBarPage.withContext(connect(s => s.member)(Mine), 2);
