import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import { Button, List, InputItem, TextareaItem, WhiteSpace, Toast } from 'antd-mobile';
import { VerticalPage } from '../../components/Layout';
import Avatar from '../../components/Mine/Avatar';
import { flattenError } from '../../utils';

const { Header, Body, Footer } = VerticalPage;

class MineDetail extends PureComponent {
  /* eslint-disable no-undef */
  state = {
    errors: [],
    loading: false,
  };

  showError = prop => () => {
    const err = this.state.errors.filter(x => x.field === prop)[0];
    Toast.fail(err.message, 5);
  }

  hanldeSubmit = () => {
    this.props.form.validateFields((error, value) => {
      if (error) {
        this.setState({ errors: flattenError(error) });
        return undefined;
      }

      this.setState({ loading: true });

      this.props.dispatch({
        type: 'member/updateMine',
        payload: {
          mine: value,
          error: (err) => {
            this.setState({ loading: false });
            Toast.offline(err);
          },
          success: () => {
            this.setState({ loading: false });
            Toast.success('修改成功');
          },
        },
      });
    });
  }

  render() {
    const user = this.props.mine || {};
    const { getFieldProps } = this.props.form;
    const hasError = (prop) => {
      return !!this.state.errors.filter(x => x.field === prop).length;
    };

    getFieldProps('Avatar', {
      initialValue: user.Avatar,
    });

    return (
      <VerticalPage style={{ height: '100%', overflowY: 'auto' }}>
        <Header>
          <Avatar src={user.Avatar} />
        </Header>
        <Body>
          <List renderHeader={() => '基础信息'}>
            <InputItem
              {...getFieldProps('Name', {
                initialValue: user.Name,
                rules: [{ required: true, message: '请填写姓名' }],
              })}
              clear
              error={hasError('Name')}
              onErrorClick={this.showError('Name')}
              placeholder="填写姓名"
            >姓名</InputItem>
            <InputItem
              {...getFieldProps('NickyName', {
                initialValue: user.NickyName,
              })}
              clear
              placeholder="填写昵称"
            >昵称</InputItem>
          </List>
          <WhiteSpace />
          <List renderHeader={() => '联系信息'}>
            <InputItem
              {...getFieldProps('Phone', {
                initialValue: user.Phone,
                rules: [{ required: true, message: '请填写手机号码' }],
              })}
              type="phone"
              error={hasError('Phone')}
              onErrorClick={this.showError('Phone')}
              placeholder="186 1234 5678"
            >手机号码</InputItem>
            <InputItem
              {...getFieldProps('IdCard', {
                initialValue: user.IdCard,
                rules: [{ required: true, message: '请填写身份证号码' }],
              })}
              type="number"
              maxLength={18}
              error={hasError('IdCard')}
              onErrorClick={this.showError('IdCard')}
              placeholder="身份证号码"
            >身份证</InputItem>
            <InputItem
              {...getFieldProps('Email', {
                initialValue: user.Email,
              })}
              type="email"
              placeholder="foobar@domain.com"
            >E-mail</InputItem>
            <InputItem
              {...getFieldProps('Passport', {
                initialValue: user.Passport,
              })}
              placeholder="护照号码"
            >护照号</InputItem>
            <TextareaItem
              {...getFieldProps('Addr', {
                initialValue: user.Addr,
                rules: [{ required: true, message: '请填写常住地址' }],
              })}
              title="地址"
              placeholder="输入常住地址"
              autoHeight
              clear
              error={hasError('Addr')}
              onErrorClick={this.showError('Addr')}
            />
          </List>
          <WhiteSpace />
          <List renderHeader={() => '其他'}>
            <InputItem
              {...getFieldProps('Profession', {
                initialValue: user.Profession,
              })}
              placeholder="职业"
            >职业</InputItem>
            <TextareaItem
              {...getFieldProps('Interests', {
                initialValue: user.Interests,
              })}
              title="兴趣爱好"
              placeholder="兴趣爱好"
              autoHeight
              clear
            />
          </List>
          <List renderHeader={() => '过敏史或者特殊病史'}>
            <TextareaItem
              {...getFieldProps('DiseaseHistory', {
                initialValue: user.DiseaseHistory,
              })}
              rows={3}
              autoHeight
              clear
            />
          </List>
        </Body>
        <Footer>
          <Button
            type="primary"
            loading={this.state.loading}
            disabled={this.state.loading}
            onClick={this.hanldeSubmit}
          >保存</Button>
        </Footer>
      </VerticalPage>
    );
  }
}

export default connect(s => s.member)(createForm()(MineDetail));
