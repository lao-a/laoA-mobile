import React from 'react';
import { connect } from 'dva';
import { CommPage } from '../../components/Layout';
import Result from '../../components/common/Result';

const MemberResult = (props) => {
  const result = props.result || {};

  return (
    <CommPage title={result.title} style={{ backgroundColor: '#fff' }}>
      <Result {...result} />
    </CommPage>
  );
};

export default connect(s => s.member)(MemberResult);
