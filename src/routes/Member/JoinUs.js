import React from 'react';
import { connect } from 'dva';
import MemberDetail from '../../components/Member/Detail';

class MemberJoin extends React.Component {
  /* eslint-disable no-undef */
  handleSubmit = (data) => {
    const payload = { params: data };
    this.props.dispatch({ type: 'member/', payload });
  }

  render() {
    return (
      <MemberDetail
        onSubmit={this.handleSubmit}
      />
    );
  }
}

export default connect(s => s.member)(MemberJoin);
